The program is written in Python 3.7 with Spyder IDE. In order to run it, several steps should be obtained:
 - download Python 3.7
 - download Pandas
 - download NLTK
 - download itertools
 - download bs4
 - have the .json datasets in the same folder as the python files
 
 The program is runned as a normal python program.  
 **From terminal:** python filename.py  
 Different editors may have simple **"Run"** button than can be pressed.
