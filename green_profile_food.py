import os
import json
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from bs4 import BeautifulSoup
import lxml
import string
import numpy as np
import itertools
import math

#TITLE AND DESCRIPTION EXPERIMENT FILE FOR GOURMET FOOD CATEGORY

#function for finding multiple ethical words
def search_words(row_in_matrix, word):
    count = 0
    for value in row_in_matrix:
        if word == value:
            count += 1
    return count

#lists for different terms
ethical_term_often = ['eco','bio',
                'organic','local', 'crueltyfree','natural', 
                'vegan', 'vegetarian',
                'sustainable']

ethical_term_special = ['plantbased', 'compostable', 'nongmo', 'palmoilfree', 'plasticfree', 'humane',
                'dairyfree', 'singlesource', 'beekeeping', 'nontoxic',
                'enivronmentally', 'reusable', 'renewable',
                'green', 'ecofriendly', 'biodegradable', 'recyclable',
                'shadegrown', 'birdfriendly','freerange', 'fresh', 'noncontaminated']

positive_list = ['no pesticides', 'without pesticides', 'no gmo',
                 'no palm oil', 'without palm oil', 'rainforest alliance',
                 'plastic free', 'shade grown', 'dairy free',
                 'factory farmed', 'single source', 'no preservative', 'without preservative',
                 'pasture fed', 'free range', 'bird friendly',
                 'environmentally friendly', 'non contaminated', 'board certified', 'vegan friendly', 'vegetarian friendly']

social_term = ['fairtrade', 'certified', 'ethical',
               'Blab', 'boardcertified', 'usda',
               'peta', 'stewardship', 'utz', 'rainforest', 'alliance',
               'iso', 'certificate', 'ecocert']

negative_term = ['tinned', 'toxics', 'polluting',
                  'synthethic', 'palmoil',
                 'packed', 'overseas', 'bottled', 'dairy',
                 'unsustainable', 'sungrown',
                 'factory', 'refrigerated', 'blended', 'brazilian',
                 'nonvegan', 'albacore', 'nonrecyclable', 'plastic', 'pod']

negative_list = ['anti union', 'synthetic pesticides',
                 'fish bladder', 'with gmo', 'with palm oil',
                 'plastic package', 'coffee pod', 'plastic bottle',
                 'sun grown', 'factory farmed', 'tea capsule', 'tea pod',
                 'tea bag', 'blended tea', 'brazilian soya',
                 'albacore tuna', 'not recyclable', 'coffee capsule', 'synthetic chemicals']


#variables for found ethical words
ethical_words_found_title, ethical_words_found_description = [], []

#string of punctuation marks
set_of_punc = string.punctuation
#set of stopwords
stop_words = set(stopwords.words('english'))
#remove some words from the stopwords
remove_words = set(("no", "not"))
new_stop_words = stop_words.difference(remove_words)
#lemmatizer variable
lemmatizer = WordNetLemmatizer()

#read the data from the json file
data = []
with open('meta_Grocery_and_Gourmet_Food.json') as f:
    for list_of_dicts in f:
        data.append(json.loads(list_of_dicts.strip()))

#make the dataframe
df = pd.DataFrame.from_dict(data)

#process the title
empty_one = list()
for i, string in enumerate(df['title']):
    #make the title lowercase
    df['title'][i] = df['title'][i].lower()
    #split the string by characters
    char_in_titles = list(df['title'][i])
    #create a list to accept the desired characters
    list_for_dict = list()
    #perform the filter
    for char_in_title in char_in_titles:
        if char_in_title not in set_of_punc:
            list_for_dict.append(char_in_title)

    #split the title word by word
    df['title'][i] = ' '.join([word for word in ''.join(list_for_dict).split(" ") if word != ""])
    #remove the stopwords from the title
    df['title'][i] = ' '.join([word for word in df['title'][i].split(" ") if word not in new_stop_words]).split(" ")
    #lemmatize the title
    df['title'][i] = [lemmatizer.lemmatize(word) for word in df['title'][i]]

    
#process the description
for i, array in enumerate(df['description']):
    for j, string in enumerate(df['description'][i]):
        #make the description lowercase
        input_string = df['description'][i][j].lower()
        #remove the html tags
        df['description'][i][j] = BeautifulSoup(input_string, "lxml").text
        tmp = df['description'][i][j]
        #remove the punctuation from the description
        df['description'][i][j] = ''.join([word for word in df['description'][i][j] if word not in set_of_punc])
        #remove the stopwords from the description
        df['description'][i][j] = ' '.join([word for word in df['description'][i][j].split(" ") if word not in new_stop_words]).split(" ")
        #lemmatize the description
        df['description'][i][j] = [lemmatizer.lemmatize(word) for word in df['description'][i][j]]
        

dataframe_title_column, dataframe_description_column = df['title'].tolist(), df['description'].tolist()

#merge the description to the title
merged_description_matrix = []
for i in range(len(dataframe_description_column)):
     merged_description_matrix.append(list(itertools.chain(*dataframe_description_column[i])))
 
#get all words from title and description
values_matrix = list()
ethical_words_base = list()
for i, array in enumerate(merged_description_matrix):
    values_matrix.append(len(array) + len(dataframe_title_column[i]))
    ethical_words_base.append(array + dataframe_title_column[i])
    
#count the number of terms in each list
ethical_words_often_total = list()
ethical_words_total = list()
social_words_total = list()
negative_words_total = list()

#number of often ethical terms
for array in ethical_words_base:
    count = 0
    for ethical_word in ethical_term_often:
        count += search_words(array, ethical_word)
    ethical_words_often_total.append(count)
    
#number of specific ethical terms
for array in ethical_words_base:
    count = 0
    for ethical_word_special in ethical_term_special:
        count += search_words(array, ethical_word_special)
    ethical_words_total.append(count)

#number of social terms
for array in ethical_words_base:
    count = 0
    for social_word in social_term:
        count += search_words(array, social_word)
    social_words_total.append(count)
    
#number of negative terms
for array in ethical_words_base:
    count = 0
    for negative_word in negative_term:
        count += search_words(array, negative_word)
    negative_words_total.append(count)
    
#create an output_matrix for negative and positive sentences
output_matrix = []
output_matrix_next = []

#number of negative sentences
#iterate over all the rows in ethical_words_base_list
for ethical_words_base_list in ethical_words_base:
    #create a row to put into the output matrix
    row = []
    for negative_list_string in negative_list:
        #create found words to place inside the words we find in ethical_words_base_rows
        found_words = []
        count = 0
        negative_list_array = negative_list_string.split(" ")
        #check if the word in ethical_base_row exists
        for word in ethical_words_base_list:
            if word in negative_list_array:
                found_words.append(word)
        #if the string exists then put true in the row otherwise put false
        if negative_list_string in ' '.join(found_words):
            count += 1
            row.append(count)
        elif negative_list_string not in ' '.join(found_words):
            row.append(0)
    #append the row to the output matrix when done
    output_matrix.append(sum(row))
    
#number of positive sentences
#iterate over all the rows in ethical_words_base_list
for ethical_words_base_list in ethical_words_base:
    #create a row to put into the output matrix
    row = []
    for positive_list_string in positive_list:
        #create found words to place inside the words we find in ethical_words_base_rows
        found_words = []
        count = 0
        positive_list_array = positive_list_string.split(" ")
        #check if the word in ethical_base_row exists
        for word in ethical_words_base_list:
            if word in positive_list_array:
                found_words.append(word)
        #if the string exists then put true in the row otherwise put false
        if positive_list_string in ' '.join(found_words):
            count += 1
            row.append(count)
        elif positive_list_string not in ' '.join(found_words):
            row.append(0)
    #append the row to the output matrix when done
    output_matrix_next.append(sum(row))
    

#calculate each score as a percentage
percentage_list_ethical_often = list()
percentage_list_ethical = list()     
percentage_list_social = list() 
percentage_list_negative = list()
percentage_list_negative_sent = list()
percentage_list_positive_sent = list()

#percentage frequent ethical terms
for i in range(len(ethical_words_often_total)): 
    percentage_ethical_often = (ethical_words_often_total[i]*2/values_matrix[i])*100
    percentage_list_ethical_often.append(percentage_ethical_often)
      
#percentage specific ethical terms
for i in range(len(ethical_words_total)): 
    percentage_ethical = (ethical_words_total[i]/values_matrix[i])*100
    percentage_list_ethical.append(percentage_ethical)
    
#percentage social terms
for i in range(len(social_words_total)): 
    percentage_social = (social_words_total[i]/values_matrix[i])*100
    percentage_list_social.append(percentage_social)
    
#percentage negative terms
for i in range(len(negative_words_total)): 
    percentage_negative = (negative_words_total[i]/values_matrix[i])*100
    percentage_list_negative.append(percentage_negative)

#percentage negative sentences
for i in range(len(output_matrix)): 
    percentage_negative_sent = (output_matrix[i]/values_matrix[i])*100
    percentage_list_negative_sent.append(percentage_negative_sent)

#percentage positive sentences
for i in range(len(output_matrix_next)): 
    percentage_positive_sent = (output_matrix_next[i]/values_matrix[i])*100
    percentage_list_positive_sent.append(percentage_positive_sent)

#put all lists into dataframes
df['often_ethical'] = percentage_list_ethical_often
df['specific_ethical'] = percentage_list_ethical
df['social_score'] = percentage_list_social
df['positive_sent'] = percentage_list_positive_sent
df['negative_words'] = percentage_list_negative
df['negative_sent'] = percentage_list_negative_sent

#calculate the green profile
df['sust_score'] = (df['often_ethical'] + df['specific_ethical'] + df['social_score'] + df['positive_sent'] - (df['negative_words'] + df['negative_sent']))
df['green_profile'] = (df['sust_score']/20).apply(np.ceil)
  
#sort by descending green profile and print
df = df.sort_values(by ='green_profile' , ascending=False, kind='quicksort')
print(df[['asin', 'often_ethical', 'specific_ethical', 'social_score', 'positive_sent', 'negative_words', 'negative_sent', 'green_profile']])



